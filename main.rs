use nom::types::CompleteStr;
use std::str;

use nom::space;
use nom::types::CompleteByteSlice;

//use geometry::SchemaPoint2D;
pub struct SchemaPoint2D(f32, f32);
impl SchemaPoint2D {
    fn new(x:f32, y:f32) -> SchemaPoint2D {
        SchemaPoint2D(x, y)
    }
}

fn bytes_to_utf8(c: CompleteByteSlice) -> Result<CompleteStr, str::Utf8Error> {
    str::from_utf8(c.0).map(|i| { CompleteStr(i) })
}

/// Parses a general utf8 string
named!(pub utf8_str(CompleteByteSlice) -> CompleteStr,
    map_res!(
        take_until_either!(" \r\n"),
        bytes_to_utf8
    )
);

/// Parses a utf8 numberstring value to float
named!(pub coordinate(CompleteByteSlice) -> f32,
    map_res!(number_str, { |i: &str| i.parse() })
);

named!(number_str<CompleteByteSlice, &str>,
    map_res!(do_parse!(b: take_while!(is_number_char) >> (str::from_utf8(b.0))), |r| r)
);

fn is_number_char(c: u8) -> bool {
    ((c >= '0' as u8) && (c <= '9' as u8)) || c == '-' as u8 || c == '.' as u8
}

named!(pub point<CompleteByteSlice,SchemaPoint2D>,
    do_parse!(
        x: coordinate >>
        space >>
        y: coordinate >>
        (SchemaPoint2D::new(x,y))
    )
);